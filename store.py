from entities.student import Student
from entities.course import Course
from entities.grade import Grade


class NotFoundError(Exception):
    pass


class AlreadyExistsError(Exception):
    pass


class Store:
    def __init__(self) -> None:
        self.students = dict()
        self.student_courses = list()
        self.courses = dict()

    def add_student(self, student: Student):
        if self.students.get(student.identifier) is not None:
            raise AlreadyExistsError()
        self.students.update({student.identifier: student})

    def add_course(self, course: Course):
        if self.courses.get(course.identifier) is not None:
            raise AlreadyExistsError()
        self.courses.update({course.identifier: course})

    def set_student_course_grade(self, grade: Grade):
        if self.students.get(grade.student_id) is None:
            raise NotFoundError()
        if self.courses.get(grade.course_id) is None:
            raise NotFoundError()
        student_course = self.get_student_course_grade(
            grade.student_id, grade.course_id
        )
        if student_course is not None:
            student_course.grade = grade.grade
        else:
            self.student_courses.append(grade)

    def get_student_course_grade(self, student_id, course_id):
        student_courses = [
            record
            for record in self.student_courses
            if record.course_id == course_id and record.student_id == student_id
        ]
        if len(student_courses) == 1:
            return student_courses[0]
        elif len(student_courses) == 0:
            return None
        else:
            # How is that possible to reach here?!
            return None

    def get_all_course_student_grades(self, course_id):
        return list(
            filter(
                lambda student_course: student_course.course_id == course_id,
                self.student_courses,
            )
        )

    def get_all_student_course_grades(self, student_id):
        return list(
            filter(
                lambda student_course: student_course.student_id == student_id,
                self.student_courses,
            )
        )
