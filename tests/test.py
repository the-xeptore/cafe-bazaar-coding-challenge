import sys
import os
import subprocess
from difflib import ndiff


__dir = os.path.dirname(__file__)


def test(suite_number: int) -> bool:
    with open(os.path.join(__dir, f"{suite_number}.in"), "r") as input_file:
        input_file_text = input_file.read()

    with open(os.path.join(__dir, f"{suite_number}.out"), "r") as output_file:
        output_file_text = output_file.read()

    process = subprocess.Popen(
        [sys.executable, "./main.py"], stdin=subprocess.PIPE, stdout=subprocess.PIPE
    )
    out, _ = process.communicate(input_file_text.encode())

    actual_output = out.decode().strip()
    expected_output = output_file_text

    if actual_output != expected_output:
        print(f"Test Failed At Test Suite: {suite_number}:")
        diff = ndiff(
            actual_output.splitlines(keepends=True),
            expected_output.splitlines(keepends=True),
        )
        print("".join(diff))
        return False

    return True


for i in range(7):
    if test(i) is False:
        exit(1)
