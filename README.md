# Cafe Bazaar Coding Challenge

Solution to Cafe Bazaar interview coding challenge

## Files

The [`onefile.py`](/onefile.py) module is the single filed combined version of the application which is used for <https://quera.ir> submissions.

## Usage

### Prerequisites

- `git` (`v2.31.1`)
- `python` (`v3.9.4`)
- `make` (`v4.3`) - optional

### Run Application

- with `make`:

  ```sh
  make run
  ```

- without `make`:

  ```sh
  python main.py
  ```

### Run Tests

- with `make`:

  ```sh
  make test
  ```

- without `make`:

  ```sh
  python tests/test.py
  ```
