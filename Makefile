PYTHON = python

run:
	$(PYTHON) main.py
.PHONY: run

test:
	$(PYTHON) tests/test.py
	@echo 'All tests passed ✅'
.PHONY: test
