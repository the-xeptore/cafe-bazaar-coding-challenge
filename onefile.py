"""
This is the 1-filed version of the original implementation that is used
for quera.ir execution as it only accepts one script file.

You can access the original modular implementation at:
https://gitlab.com/the-xeptore/cafe-bazaar-coding-challenge
"""

from enum import Enum


def format_default_float(number: float) -> str:
    return "{:.2f}".format(round(number, 2))


class Command(Enum):
    ADD = "ADD"
    GRADE = "GRADE"
    STATS = "STATS"
    END = "END"
    EXIT = "EXIT"


class EntityTypeKind(Enum):
    COURSE = "COURSE"
    STUDENT = "STUDENT"


class IdentifyableEntity:
    def __init__(self, identifier: str) -> None:
        self.identifier = identifier


class Student(IdentifyableEntity):
    pass


class Course(IdentifyableEntity):
    pass


class Grade:
    def __init__(self, course_id, student_id, grade) -> None:
        self.course_id = course_id
        self.student_id = student_id
        self.grade = grade


class NotFoundError(Exception):
    pass


class AlreadyExistsError(Exception):
    pass


class Store:
    def __init__(self) -> None:
        self.students = dict()
        self.student_courses = list()
        self.courses = dict()

    def add_student(self, student: Student):
        if self.students.get(student.identifier) is not None:
            raise AlreadyExistsError()
        self.students.update({student.identifier: student})

    def add_course(self, course: Course):
        if self.courses.get(course.identifier) is not None:
            raise AlreadyExistsError()
        self.courses.update({course.identifier: course})

    def set_student_course_grade(self, grade: Grade):
        if self.students.get(grade.student_id) is None:
            raise NotFoundError()
        if self.courses.get(grade.course_id) is None:
            raise NotFoundError()
        student_course = self.get_student_course_grade(
            grade.student_id, grade.course_id
        )
        if student_course is not None:
            student_course.grade = grade.grade
        else:
            self.student_courses.append(grade)

    def get_student_course_grade(self, student_id, course_id):
        student_courses = [
            record
            for record in self.student_courses
            if record.course_id == course_id and record.student_id == student_id
        ]
        if len(student_courses) == 1:
            return student_courses[0]
        elif len(student_courses) == 0:
            return None
        else:
            # How is that possible to reach here?!
            return None

    def get_all_course_student_grades(self, course_id):
        return list(
            filter(
                lambda student_course: student_course.course_id == course_id,
                self.student_courses,
            )
        )

    def get_all_student_course_grades(self, student_id):
        return list(
            filter(
                lambda student_course: student_course.student_id == student_id,
                self.student_courses,
            )
        )


class AddCommandArgs:
    def __init__(self, type_kind, identifier) -> None:
        self.type_kind = type_kind
        self.identifier = identifier


class AddCommandHandler:
    def __init__(self, store: Store) -> None:
        self.store = store

    def handle(self, args: AddCommandArgs):
        if args.type_kind == EntityTypeKind.COURSE.value:
            course_id = args.identifier
            self.store.add_course(Course(course_id))
        elif args.type_kind == EntityTypeKind.STUDENT.value:
            student_id = args.identifier
            self.store.add_student(Student(student_id))
        else:
            # How is it possible to even reach here?!
            pass


class GradeCommandArgs:
    def __init__(self, course_id, student_id, grade) -> None:
        self.course_id = course_id
        self.student_id = student_id
        self.grade = grade


class GradeCommandHandler:
    def __init__(self, store: Store) -> None:
        self.store = store

    def handle(self, args: GradeCommandArgs):
        student_course_grade = Grade(args.course_id, args.student_id, args.grade)
        try:
            self.store.set_student_course_grade(student_course_grade)
        except NotFoundError:
            print("INVALID DATA")


class StatsCommandArgs:
    def __init__(self, type_kind, identifier) -> None:
        self.type_kind = type_kind
        self.identifier = identifier


class StatsCommandHandler:
    def __init__(self, store: Store) -> None:
        self.store = store

    def handle(self, args: StatsCommandArgs):
        if args.type_kind == EntityTypeKind.STUDENT.value:
            student_id = args.identifier
            self.handle_student_courses_stats(student_id)
        elif args.type_kind == EntityTypeKind.COURSE.value:
            course_id = args.identifier
            self.handle_course_students_stats(course_id)

    def handle_student_courses_stats(self, student_id):
        student_course_grades = self.store.get_all_student_course_grades(student_id)
        for student_course_grade in self.sort_student_course_grades(
            student_course_grades
        ):
            print(
                student_course_grade.course_id,
                self.format_print_grade(student_course_grade.grade),
            )
        grades_average = self.calculate_student_course_grades_average(
            student_course_grades
        )
        print(self.format_print_grade(grades_average))

    def handle_course_students_stats(self, course_id):
        course_student_grades = self.store.get_all_course_student_grades(course_id)
        for course_student_grade in self.sort_course_students_grades(
            course_student_grades
        ):
            print(
                course_student_grade.student_id,
                self.format_print_grade(course_student_grade.grade),
            )
        grades_average = self.calculate_course_student_grades_average(
            course_student_grades
        )
        print(self.format_print_grade(grades_average))

    @staticmethod
    def sort_student_course_grades(course_student_grades):
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.course_id
        )
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.grade, reverse=True
        )
        return course_student_grades

    @staticmethod
    def calculate_student_course_grades_average(student_course_grades) -> float:
        number_of_student_courses = len(student_course_grades)
        if number_of_student_courses == 0:
            return 0.0
        return (
            sum(
                map(
                    lambda student_course_grade: student_course_grade.grade,
                    student_course_grades,
                )
            )
            / number_of_student_courses
        )

    @staticmethod
    def sort_course_students_grades(course_student_grades):
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.student_id
        )
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.grade, reverse=True
        )
        return course_student_grades

    @staticmethod
    def calculate_course_student_grades_average(course_student_grades) -> float:
        number_of_course_students = len(course_student_grades)
        if number_of_course_students == 0:
            return 0.0
        return (
            sum(
                map(
                    lambda course_student_grade: course_student_grade.grade,
                    course_student_grades,
                )
            )
            / number_of_course_students
        )

    @staticmethod
    def format_print_grade(grade):
        return format_default_float(grade)


class App:
    def __init__(self) -> None:
        self.store = Store()
        self.add_command_handler = AddCommandHandler(self.store)
        self.grade_command_handler = GradeCommandHandler(self.store)
        self.stats_command_handler = StatsCommandHandler(self.store)
        self.handlers = {
            Command.ADD.value: self.handle_add_sub_command,
            Command.GRADE.value: self.handle_grade_sub_command,
            Command.STATS.value: self.handle_stats_sub_command,
        }

    def start(self):
        while True:
            command = input()
            handler = self.handlers.get(command)
            if handler is not None:
                handler()
            else:
                break

    def handle_add_sub_command(self):
        total_added_entities = 0
        while True:
            sub_command = input()
            if sub_command == Command.END.value:
                break
            type_kind, identifier = sub_command.split()
            sub_command_args = AddCommandArgs(type_kind, identifier)
            try:
                self.add_command_handler.handle(sub_command_args)
                total_added_entities += 1
            except AlreadyExistsError:
                pass
        print(total_added_entities)

    def handle_grade_sub_command(self):
        while True:
            sub_command = input()
            if sub_command == Command.END.value:
                break
            course_id, student_id, grade = sub_command.split()
            sub_command_args = GradeCommandArgs(course_id, student_id, float(grade))
            self.grade_command_handler.handle(sub_command_args)

    def handle_stats_sub_command(self):
        while True:
            sub_command = input()
            if sub_command == Command.END.value:
                break
            type_kind, identifier = sub_command.split()
            sub_command_args = StatsCommandArgs(type_kind, identifier)
            self.stats_command_handler.handle(sub_command_args)


def main():
    app = App()
    app.start()


if __name__ == "__main__":
    main()
