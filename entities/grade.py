class Grade:
    def __init__(self, course_id, student_id, grade) -> None:
        self.course_id = course_id
        self.student_id = student_id
        self.grade = grade
