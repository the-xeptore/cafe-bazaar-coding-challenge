from enum import Enum


class Command(Enum):
    ADD = "ADD"
    GRADE = "GRADE"
    STATS = "STATS"
    END = "END"
    EXIT = "EXIT"

class EntityTypeKind(Enum):
    COURSE = "COURSE"
    STUDENT = "STUDENT"
