from constants import Command
from store import Store, AlreadyExistsError
from commands.add import AddCommandArgs, AddCommandHandler
from commands.grade import GradeCommandArgs, GradeCommandHandler
from commands.stats import StatsCommandArgs, StatsCommandHandler


class App:
    def __init__(self) -> None:
        self.store = Store()
        self.add_command_handler = AddCommandHandler(self.store)
        self.grade_command_handler = GradeCommandHandler(self.store)
        self.stats_command_handler = StatsCommandHandler(self.store)
        self.handlers = {
            Command.ADD.value: self.handle_add_sub_command,
            Command.GRADE.value: self.handle_grade_sub_command,
            Command.STATS.value: self.handle_stats_sub_command,
        }

    def start(self):
        while True:
            command = input()
            handler = self.handlers.get(command)
            if handler is not None:
                handler()
            else:
                break

    def handle_add_sub_command(self):
        total_added_entities = 0
        while True:
            sub_command = input()
            if sub_command == Command.END.value:
                break
            type_kind, identifier = sub_command.split()
            sub_command_args = AddCommandArgs(type_kind, identifier)
            try:
                self.add_command_handler.handle(sub_command_args)
                total_added_entities += 1
            except AlreadyExistsError:
                pass
        print(total_added_entities)

    def handle_grade_sub_command(self):
        while True:
            sub_command = input()
            if sub_command == Command.END.value:
                break
            course_id, student_id, grade = sub_command.split()
            sub_command_args = GradeCommandArgs(course_id, student_id, float(grade))
            self.grade_command_handler.handle(sub_command_args)

    def handle_stats_sub_command(self):
        while True:
            sub_command = input()
            if sub_command == Command.END.value:
                break
            type_kind, identifier = sub_command.split()
            sub_command_args = StatsCommandArgs(type_kind, identifier)
            self.stats_command_handler.handle(sub_command_args)
