def format_default_float(number: float) -> str:
    return round(number, 2)
