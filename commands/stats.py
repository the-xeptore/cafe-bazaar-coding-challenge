from utils import format_default_float
from constants import EntityTypeKind
from store import Store


class StatsCommandArgs:
    def __init__(self, type_kind, identifier) -> None:
        self.type_kind = type_kind
        self.identifier = identifier


class StatsCommandHandler:
    def __init__(self, store: Store) -> None:
        self.store = store

    def handle(self, args: StatsCommandArgs):
        if args.type_kind == EntityTypeKind.STUDENT.value:
            student_id = args.identifier
            self.handle_student_courses_stats(student_id)
        elif args.type_kind == EntityTypeKind.COURSE.value:
            course_id = args.identifier
            self.handle_course_students_stats(course_id)

    def handle_student_courses_stats(self, student_id):
        student_course_grades = self.store.get_all_student_course_grades(student_id)
        for student_course_grade in self.sort_student_course_grades(
            student_course_grades
        ):
            print(
                student_course_grade.course_id,
                self.format_print_grade(student_course_grade.grade),
            )
        grades_average = self.calculate_student_course_grades_average(
            student_course_grades
        )
        print(self.format_print_grade(grades_average))

    def handle_course_students_stats(self, course_id):
        course_student_grades = self.store.get_all_course_student_grades(course_id)
        for course_student_grade in self.sort_course_students_grades(
            course_student_grades
        ):
            print(
                course_student_grade.student_id,
                self.format_print_grade(course_student_grade.grade),
            )
        grades_average = self.calculate_course_student_grades_average(
            course_student_grades
        )
        print(self.format_print_grade(grades_average))

    @staticmethod
    def sort_student_course_grades(course_student_grades):
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.course_id
        )
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.grade, reverse=True
        )
        return course_student_grades

    @staticmethod
    def calculate_student_course_grades_average(student_course_grades) -> float:
        number_of_student_courses = len(student_course_grades)
        if number_of_student_courses == 0:
            return 0.0
        return (
            sum(
                map(
                    lambda student_course_grade: student_course_grade.grade,
                    student_course_grades,
                )
            )
            / number_of_student_courses
        )

    @staticmethod
    def sort_course_students_grades(course_student_grades):
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.student_id
        )
        course_student_grades.sort(
            key=lambda course_student_grade: course_student_grade.grade, reverse=True
        )
        return course_student_grades

    @staticmethod
    def calculate_course_student_grades_average(course_student_grades) -> float:
        number_of_course_students = len(course_student_grades)
        if number_of_course_students == 0:
            return 0.0
        return (
            sum(
                map(
                    lambda course_student_grade: course_student_grade.grade,
                    course_student_grades,
                )
            )
            / number_of_course_students
        )

    @staticmethod
    def format_print_grade(grade):
        return format_default_float(grade)
