from store import Store, NotFoundError
from entities.grade import Grade


class GradeCommandArgs:
    def __init__(self, course_id, student_id, grade) -> None:
        self.course_id = course_id
        self.student_id = student_id
        self.grade = grade


class GradeCommandHandler:
    def __init__(self, store: Store) -> None:
        self.store = store

    def handle(self, args: GradeCommandArgs):
        try:
            student_course_grade = Grade(args.course_id, args.student_id, args.grade)
            self.store.set_student_course_grade(student_course_grade)
        except NotFoundError:
            print("INVALID DATA")
