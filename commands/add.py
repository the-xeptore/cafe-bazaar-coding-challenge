from store import Store
from constants import EntityTypeKind
from entities.student import Student
from entities.course import Course


class AddCommandArgs:
    def __init__(self, type_kind, identifier) -> None:
        self.type_kind = type_kind
        self.identifier = identifier


class AddCommandHandler:
    def __init__(self, store: Store) -> None:
        self.store = store

    def handle(self, args: AddCommandArgs):
        if args.type_kind == EntityTypeKind.COURSE.value:
            course_id = args.identifier
            self.store.add_course(Course(course_id))
        elif args.type_kind == EntityTypeKind.STUDENT.value:
            student_id = args.identifier
            self.store.add_student(Student(student_id))
        else:
            # How is it possible to even reach here?!
            pass
